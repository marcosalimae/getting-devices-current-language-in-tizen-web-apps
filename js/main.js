/*
 * Get the application namespace.
 */
var getting_current_language = window.getting_current_language || {};

/*
 * Main module.
 */
getting_current_language.Main = ( function () {

	/*
	 * Message error for log when Tizen API is not available.
	 */
	var TIZEN_ERROR_MESSAGE = "Tizen API not found.";

	/*
	 * Span tag element.
	 */
	var lang = null;
	
	/*
	 * Configures action for Tizen's device back button.
	 */
	var _configureDeviceBackButton = function () {
		document.addEventListener('tizenhwkey', function(eventKey) {
			if (eventKey.keyName === "back") {
				tizen.application.getCurrentApplication().exit();
			}
		});
	};

	/*
	 * onGetLocaleError callback.
	 */
	var _onGetLocaleError = function (error) {
		console.log(error);
	};

	/*
	 * onGetLocaleSuccess callback.
	 */
	var _onGetLocaleSuccess = function (locale) {
		lang = document.getElementById("lang");
		lang.innerHTML = locale.language;
	};

	/*
	 * onLocaleChangedSuccess callback.
	 */
	var _onLocaleChangedSuccess = function (locale) {
		lang.innerHTML = locale.language;
	};

	/*
	 * onLocaleChangedError callback.
	 */
	var _onLocaleChangedError = function (error) {
		console.log(error);
	};

	/*
	 * Initializes main module.
	 */
	var _init = function () {
		lang = document.getElementById("lang");

		tizen.systeminfo.getPropertyValue("LOCALE", _onGetLocaleSuccess,_onGetLocaleError);
		tizen.systeminfo.addPropertyValueChangeListener("LOCALE",_onLocaleChangedSuccess, _onLocaleChangedError);
		_configureDeviceBackButton();
	
	};

	return {
		init : _init,
	};

}());
window.onload = getting_current_language.Main.init();